// @flow

import { 
  PICTURE_DETAILS_FETCH_REQUESTED,
  PICTURE_DETAILS_FETCH_SUCCESS,
} from './actions'
import { FETCH_FAILED } from '../HomeContainer/actions';

const initialState: State = {
  hiResPictures: {},
  isLoading: false,
  errorMessage: '',
}

type State = {|
  hiResPictures: Object,
  isLoading: boolean,
  errorMessage: string,
|}


export default function (state: State = initialState, action: Object) {
  const { type, payload } = action

  switch (type) {
    case PICTURE_DETAILS_FETCH_REQUESTED:
      return {
        ...state,
        isLoading: true,
      }

    case PICTURE_DETAILS_FETCH_SUCCESS:
      const { imageId, hiResImage } = payload
      return {
        ...state,
        hiResPictures: {
          ...state.hiResPictures,
          [imageId]: hiResImage,
        },
        isLoading: false,
        errorMessage: '',
      }
    case FETCH_FAILED:
      return {
        ...state,
        isLoading: false,
        errorMessage: payload,
      }
    default: 
      return state;
  }
}