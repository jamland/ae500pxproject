// @flow

import * as React from 'react'
import { connect } from 'react-redux'
import { 
  Platform, 
  StatusBar,
  Share,
  ActivityIndicator,
  View,
  StyleSheet,
} from 'react-native'

import DetailView from '../../screens/DetailView'
import { fetchPictureDetails } from './actions'
import { selectHiResImage } from './selectors'

export interface Props {
  navigation: any,
  fetchPictureDetails: Function,
  isLoading: boolean,
  hiResImage: Function,
}
export interface State {
  imageUrl: string,
  filter: boolean,
}

const APPBAR_HEIGHT = 56;
const statusBarColor = '#000'
class DetailViewContainer extends React.Component<Props, State> {
  static navigationOptions = {
    headerStyle: {
      backgroundColor: statusBarColor,
      height: APPBAR_HEIGHT,
      borderBottomWidth: 0,
    },
    headerTintColor: '#fff',
  }

  state = {
    imageUrl: '',
    filter: false,
  }

  constructor (props) {
    super(props)
    StatusBar.setBarStyle('light-content')
    Platform.OS === 'android' && StatusBar.setBackgroundColor(statusBarColor)
  }

  componentDidMount () {
    const { navigation, fetchPictureDetails } = this.props
    const { pictureDetails } = navigation.state.params
    if (!this.props.hiResImage(pictureDetails.id)) {
      fetchPictureDetails(pictureDetails.id)
    }
  }

  share = async (imageId: number): Promise<any> => {
    const { pictureDetails } = this.props.navigation.state.params
    const imageUrl = pictureDetails.url
    const text = pictureDetails.title || ''

    try {
      const result = await Share.share({
        message: `👀📸 ${text} ${imageUrl}`,
      })
    } catch (error) {
      alert(error.message);
    }
  };

  applyFilter = (type): void => {
    // TODO: implement apply image filter function
    this.setState({ filter: !this.state.filter})
  }

  render () {
    const { pictureDetails: navParams } = this.props.navigation.state.params
    const { isLoading, hiResImage } = this.props
    const { filter } = this.state;

    const pictureDetails = hiResImage(navParams.id)
    
    if (!pictureDetails) return (
      <View style={styles.container}>
        <ActivityIndicator size='large' color='#ff' />
      </View>
    )
    
    const imageURL = pictureDetails.full_picture
    return (
      <DetailView
        imageUrl={imageURL}
        pictureDetails={pictureDetails}
        shareCallback={this.share}
        isLoading={isLoading}
        applyFilterCallback={this.applyFilter}
        filter={filter}
      />
    )
  }
}


const styles: any = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

function bindAction (dispatch) {
  return {
    fetchPictureDetails: imageId => dispatch(fetchPictureDetails(imageId)),
  }
}

const mapStateToProps = state => ({
  hiResImage: imageId => selectHiResImage(state, imageId),
  isLoading: state.detailViewReducer.isLoading,
})

export default connect(mapStateToProps, bindAction)(DetailViewContainer)
