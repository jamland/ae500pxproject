// @flow

export const selectHiResImage = (state: Object, imageId: number) => {
  return state.detailViewReducer.hiResPictures[imageId]
}
