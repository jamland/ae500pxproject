// @flow

import type {
  Dispatch
} from 'redux';
import { getPictures } from '../../services/API'
import { setUnauthorized } from '../Auth/actions'
import type { ActionWithPayload, ActionWithoutPayload } from '../../types/actions'

export const PICTURES_FETCH_REQUESTED = 'PICTURES_FETCH_REQUESTED'
export const PICTURES_FETCH_SUCCESS = 'PICTURES_FETCH_SUCCESS'
export const FETCH_FAILED = 'FETCH_FAILED'

export function listIsLoading (): ActionWithoutPayload {
  return {
    type: PICTURES_FETCH_REQUESTED,
  }
}

export function fetchListSuccess (data: Array<Object>, page: number): ActionWithPayload {
  return {
    type: PICTURES_FETCH_SUCCESS,
    payload: { data, page },
  }
}

export function fetchListFailed (errorMessage: string): ActionWithPayload {
  return {
    type: FETCH_FAILED,
    payload: errorMessage,  
  }
}

export function fetchPictures (page: number = 1) {
  return async (dispatch: Dispatch) => {
    dispatch(listIsLoading())

    getPictures(page)
      .then(data => { 
        if (data.status === 'Unauthorized') {
          dispatch(setUnauthorized()) 
        } else {
          dispatch(fetchListSuccess(data, page)) 
        }
      })
      .catch(err => { 
        dispatch(fetchListFailed('Error happened :<(')) 
      })
  }
}