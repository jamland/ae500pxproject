// @flow
import * as React from 'react'
import { Platform, StatusBar } from 'react-native'
import { connect } from 'react-redux'
import { SafeAreaView } from 'react-navigation';

import HomeView from '../../screens/Home'
import { fetchPictures } from './actions'

export interface Props {
  navigation: any,
  fetchPictures: Function,
  pictures: Array<Object>,
  isLoading: boolean,
  page: number,
}

export interface State {}

const APPBAR_HEIGHT = 56;
const statusBarColor = '#2e3ca5';
const headerBgColor = '#3c4ebc';

if (Platform.OS === 'ios') {
  SafeAreaView.setStatusBarHeight(0);
}

class HomeContainer extends React.Component<Props, State> {
  static navigationOptions = {
    title: 'Home',
    headerStyle: {
      backgroundColor: headerBgColor,
      height: APPBAR_HEIGHT,
      borderBottomWidth: 0,
    },
    headerTintColor: '#fff',
  }

  constructor (props) {
    super(props)
    StatusBar.setBarStyle('light-content')
    Platform.OS === 'android' && StatusBar.setBackgroundColor(statusBarColor)
  }

  componentDidMount () {
    this.onRefresh();
  }

  onRefresh = (): void => {
    this.props.fetchPictures(1)
  }

  onLoadNext = (): void => {
    const { page, isLoading } = this.props
    if (!isLoading) this.props.fetchPictures(page+1);
  }

  render () {
    return (
      <HomeView 
        {...this.props}
        onRefresh={this.onRefresh}
        onLoadNext={this.onLoadNext} 
      />
    )
  }
}

function bindAction (dispatch) {
  return {
    fetchPictures: page => dispatch(fetchPictures(page)),
  }
};

const mapStateToProps = state => ({
  pictures: state.homeReducer.pictures,
  page: state.homeReducer.page,
  isLoading: state.homeReducer.isLoading,
})

export default connect(mapStateToProps, bindAction)(HomeContainer)