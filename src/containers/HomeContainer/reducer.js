// @flow

import { 
  PICTURES_FETCH_SUCCESS,
  PICTURES_FETCH_REQUESTED,
  FETCH_FAILED,  
} from './actions'

type State = {|
  pictures: [] | [Object],
  isLoading: boolean,
  page: number,
  errorMessage: string,
  lastRequest?: Object,
|}

export const initialState: State = {
  pictures: [],
  isLoading: true,
  page: 1,
  errorMessage: '',
  lastRequest: null,
}

export default function (state: State = initialState, action: Object) {
  const { type, payload } = action

  switch (type) {
    case PICTURES_FETCH_REQUESTED:
      return {
        ...state,
        isLoading: true,
      }

    case PICTURES_FETCH_SUCCESS:
      const { data, page } = payload
      const prevPicState = page === 1 ? [] : state.pictures
      const nextPicState = [
        ...prevPicState,
        ...data.pictures,
      ]
      
      return {
        ...state,
        pictures: nextPicState,
        page,
        isLoading: false,
        errorMessage: '',
        lastRequest: data,
      }

    case FETCH_FAILED:
      return {
        ...state,
        isLoading: false,
        errorMessage: payload,
      }
      
    default: 
      return state;
  }
}