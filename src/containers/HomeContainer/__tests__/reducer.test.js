import reducer, { initialState } from '../reducer'
import { 
  PICTURES_FETCH_SUCCESS,
  PICTURES_FETCH_REQUESTED,
  FETCH_FAILED,  
} from '../actions'

const pictures = [
  {
    cropped_picture: "http://195.39.233.28:8035/pictures/cropped/03 - WPIX_86.jpg",
    id: "6829ba3db2feef42750c",
  },
  {
    cropped_picture: "http://195.39.233.28:8035/pictures/cropped/04 - WPIX_86.jpg",
    id: "494b1b4b176b8b9c4e50",
  }
]

const picResponse = {
  hasMore: true,
  page: 2,
  pageCount: 26,
  pictures,
}

describe('list reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      pictures: [],
      isLoading: true,
      page: 1,
      errorMessage: '',
      lastRequest: null,
    })
  })

  it('set correct state on fetch success', () => {
    expect(reducer(
      undefined,
      {
        type: PICTURES_FETCH_SUCCESS,
        payload: {
          data: picResponse,
          page: 1,
        },
      }
    )).toEqual({
      pictures: [
        ...pictures,
      ],
      isLoading: false,
      page: 1,
      errorMessage: '',
      lastRequest: picResponse,
    })
  })

  it('set correct state on fetch requested', () => {
    expect(reducer(
      undefined,
      {
        type: FETCH_FAILED,
        payload: 'error',
      }
    )).toEqual({
      ...initialState,
      isLoading: false,
      errorMessage: 'error',
    })
  })

  it('set correct state on fetch fail', () => {
    expect(reducer(
      undefined,
      {
        type: PICTURES_FETCH_REQUESTED,
      }
    )).toEqual({
      ...initialState,
      isLoading: true,
    })
  })
})
