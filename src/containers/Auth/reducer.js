// @flow

import { 
    AUTHORISED,
    NOT_AUTHORISED,
    AUTHORISATION_FAILED, 
} from './actions'

type State = {|
  loggedIn: boolean,
  error: boolean,
|}

const initialState: State = {
  loggedIn: false,
  error: false,
}


export default function (state: State = initialState, action: Object) {
  const { type, payload } = action

  switch (type) {
    case AUTHORISED:
      return {
        ...state,
        loggedIn: true,
        error: false,
      }

    case NOT_AUTHORISED:
      return {
        ...state,
        loggedIn: false,
      }

    case AUTHORISATION_FAILED:
      return {
        ...state,
        loggedIn: false,
        error: true,
      }

    default: 
      return state;
  }
}