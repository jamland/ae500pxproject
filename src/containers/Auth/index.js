// @flow
import * as React from 'react'
import { 
  View, 
  Text, 
  ActivityIndicator, 
  StyleSheet,
  AsyncStorage,
} from 'react-native'
// import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux'

import { checkAuth } from '../Auth/actions'
import { authUser } from '../../services/API'

export interface Props {
  children: any,
  loggedIn: boolean,
  error: boolean,
  checkAuth: Function,
  setAuth: Function,
}

export interface State {}

class AuthHOC extends React.Component<Props, State> {

  async componentDidMount () {
    await AsyncStorage.setItem('token', '')
    const token = await AsyncStorage.getItem('token')
    this.props.checkAuth(token)

    if (!token) {
      const authData = await authUser()
      await AsyncStorage.setItem('token', authData.token)
      this.props.checkAuth(authData.token)
    }
  }

  render () {
    const { loggedIn, error } = this.props;

    if (error) {
      return (
        <View style={styles.container}>
          <Text style={styles.error}>Authorisation Issue 🔒😟</Text>
        </View>
      )
    }

    if (!loggedIn) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size='large' color='#ff' />
        </View>
      )
    }

    if (loggedIn) return this.props.children;

    return null;
  }
}


const styles: any = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  error: {
    color: '#fff',
  }
})

function bindAction (dispatch) {
  return {
    checkAuth: (token) => dispatch(checkAuth(token)),
  }
};

const mapStateToProps = state => ({
  loggedIn: state.authReducer.loggedIn,
  error: state.authReducer.error,
})

export default connect(mapStateToProps, bindAction)(AuthHOC)