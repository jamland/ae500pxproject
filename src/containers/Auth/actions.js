// @flow

import type {
    Dispatch
  } from 'redux';

import type { ActionWithPayload, ActionWithoutPayload } from '../../types/actions'

export const AUTHORISED = 'AUTHORISED'
export const NOT_AUTHORISED = 'NOT_AUTHORISED'
export const AUTHORISATION_FAILED = 'AUTHORISATION_FAILED'

export function checkAuth (token?: string): ActionWithoutPayload {
  if (token) 
    return {
        type: AUTHORISED,
    };
  else 
    return {
      type: NOT_AUTHORISED,
    }
}

export function setUnauthorized (): ActionWithoutPayload {
  return {
    type: AUTHORISATION_FAILED,
  }
}