// @flow

import { AsyncStorage } from 'react-native'

const API_KEY = '23567b218376f79d9415' // other valid API keys: '760b5fb497225856222a', '0e2a751704a65685eefc'
const API_ENDPOINT = 'http://195.39.233.28:8035'

export const authUser = async (): Promise<Object> => {
  const url = API_ENDPOINT+'/auth'
  const body = JSON.stringify({
    'apiKey': API_KEY,
  })
  const failedResponse = {
    auth: false,
    token: null,
  }

  return await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json",
    },
    body,
  })
  .then( result => result.json())
  .catch( err => ({
    auth: false,
    token: null,
    error: err,
  }))
}

export async function getPictures (page: number = 1): Promise<Object> {
  const headers = await authHeader()
  const requestOptions = {
    method: 'GET',
    headers,
  };

  return await fetch(API_ENDPOINT+`/images?page=${page}`, requestOptions)
    .then( r => r.json())
}

export async function getPictureDetails (id: number): Object {
  const headers = await authHeader()
  const requestOptions = {
    method: 'GET',
    headers,
  };

  return await fetch(API_ENDPOINT+`/images/${id}`, requestOptions)
    .then( r => r.json())
}


export async function authHeader () {
  const token = await AsyncStorage.getItem('token')

  if (token) {
      return { 'Authorization': 'Bearer ' + token }
  } else {
      return {}
  }
}