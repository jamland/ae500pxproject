import * as React from 'react'
import {
  TouchableOpacity,
  Image,
} from 'react-native'
import FastImage from 'react-native-fast-image'

import styles from '../styles'

type Props = {
  imageUrl: string,
  imageId: number,
  openPicture: Function,
  imageStyle: Object,
}

class ListItem extends React.PureComponent<Props> {
  render () {
    const { imageUrl, imageId, openPicture, imageStyle } = this.props
    return (
      <TouchableOpacity
        onPress={() => openPicture(imageId)}
        style={styles.item}
        key={imageId}
      > 
        <FastImage
          style={imageStyle}
          resizeMode='cover'
          source={{
            uri: imageUrl,
            priority: FastImage.priority.normal,
            // headers: { Authorization: 'someAuthToken' },
          }} 
        />


      </TouchableOpacity>
    )
  }
}

export default ListItem
