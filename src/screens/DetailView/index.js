// @flow
import * as React from 'react'
import {
  View,
  Dimensions,
} from 'react-native'
import Image from 'react-native-image-progress'
import ProgressBar from 'react-native-progress/Bar'
import ImageZoom from 'react-native-image-pan-zoom'

import styles from './styles'
import DetailsFooter from './components/DetailsFooter'

type Props = {
  imageUrl: string,
  isLoading: boolean,
  shareCallback: Function,
  applyFilterCallback: Function,
  pictureDetails: Object,
  filter: boolean,
}

// TODO: it would be great to see here loader, pinch to zoom here and pan
const headerBgColor = '#3c4ebc';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
class DetailView extends React.PureComponent<Props> {
  render () {
    const { 
      imageUrl, 
      isLoading, 
      shareCallback, 
      applyFilterCallback, 
      pictureDetails,
      filter,
    } = this.props
    const filterAmount = filter ? 4 : 0

    return (
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <ImageZoom 
            cropWidth={width}
            cropHeight={height}
            imageWidth={width}
            imageHeight={height}
          >

              <Image
                source={{uri: imageUrl}}
                style={{flex:1, alignSelf: 'stretch'}}
                resizeMode="contain"
                blurRadius={filterAmount}
                indicator={ProgressBar} 
                indicatorProps={{
                  color: headerBgColor,
                }}
              />

          </ImageZoom>
        </View>
        <DetailsFooter
          pictureDetails={pictureDetails}
          shareCallback={shareCallback}
          applyFilterCallback={applyFilterCallback}
        />
      </View>
    )
  }
}

export default DetailView
