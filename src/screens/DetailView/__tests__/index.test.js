import React from 'react'
import DetailView from '../index'
// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer'

const applyFilterCallback = jest.fn()
const shareCallback = jest.fn()
const imageUrl = 'https://drscdn.500px.org/photo/247377659/m%3D900_k%3D1_a%3D1/v2?client_application_id=27071&webp=true&sig=b5b4070666461be985e7890d9c3b26054ae52c8c882fe9b6120bb2c627b53204'
const isLoading = false
const pictureDetails = { id: '222' }
const filter = true

describe('<DetailsView />', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
      <DetailView
        imageUrl={imageUrl}
        isLoading={false}
        applyFilterCallback={applyFilterCallback}
        shareCallback={shareCallback} 
        pictureDetails={pictureDetails}
        filter={filter}
      />
    ).toJSON()
    
    expect(tree).toMatchSnapshot()
  })

  // it('Render correct props', () => {
  //   const wrapper = renderer.create(
  //     <DetailView
  //       imageUrl={imageUrl}
  //       isLoading={false}
  //       applyFilterCallback={applyFilterCallback}
  //       shareCallback={shareCallback} 
  //       pictureDetails={pictureDetails}
  //       filter={filter}
  //     />
  //   ).toJSON()

  //   const props = wrapper.props;

  //   expect(props.imageUrl).toEqual(imageUrl);
  //   expect(props.isLoading).toEqual(isLoading);
  //   expect(props.shareCallback).toEqual(shareCallback);
  //   expect(props.applyFilterCallback).toEqual(applyFilterCallback);
  //   expect(props.shareCallback).toEqual(shareCallback);
  //   expect(props.filter).toEqual(filter);
  //   expect(props.pictureDetails).toEqual(pictureDetails);
  // });
})