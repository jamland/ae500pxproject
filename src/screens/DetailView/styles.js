import { Dimensions, StyleSheet } from 'react-native'
const { width } = Dimensions.get('window')

const styles: any = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    flex: 1,
  },
  imageContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStyle: {
    flex: 1,
    width: width * 0.9,
    height: width * 0.9,
  },
  backButton: {
    position: 'absolute',
    left: 5,
    top: 5,
  },
  spinner: {
    position: 'absolute',
  },
  footerContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#000',
    position: 'absolute',
    bottom: 10,
    right: 20,
    left: 20,
  },
  detailView: {
    flex: 0,
    flexDirection: 'row',
  },
  detailViewImage: {
    width: 50,
    height: 50,
    marginLeft: 'auto',
  },
  detailText: {
    flex: 1,
  },
  author: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 20,
  },
  camera: {
    color: '#fff'
  },
})
export default styles
