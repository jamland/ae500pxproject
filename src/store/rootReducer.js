import { combineReducers } from 'redux'

import authReducer from '../containers/Auth/reducer'
import homeReducer from '../containers/HomeContainer/reducer'
import detailViewReducer from '../containers/DetailViewContainer/reducer'

export default combineReducers({
  authReducer,
  homeReducer,
  detailViewReducer,
})
