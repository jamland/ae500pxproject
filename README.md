## Development Notes

### Task Description

https://agileengine.bitbucket.io/rnBsQqoEDeHMvLfR/


### RN Skeleton

 Были некоторые сложности с запуском RN Skeleton.

-   initial build failed.    
    Обновил версии node modules

-  опечатки в названии  API файлов    
`src/containers/HomeContainer/actions.js:1`
`src/containers/DetailViewContainer/actions.js:3`


### BUILD

```
npm install

```

## RUN

```
react-native run-ios
```

## DETAILS

I see all points done except:
- (optional) Swipe left/right to switch to the next/previous photo.
- (optional) Besides that, user should be able to apply list of standard image filters, via floating action buttons...

For the filters, only Blur implemented as the example, I see other can be quicky added via some plugins.


### Auth

Auth made with HOC over navigation.    
If request failed because of wrong token then 'Authorisation Issue' error shown to user. There are no handler for wrong `API_KEY` implemented or other error cases.

### Tests

Not everything  is tested. You can check this files for showcase:     
`src/containers/HomeContainer/__tests__/reducer.test.js`    
`src/screens/DetailView/__tests__/index.test.js`
I would say, tests can be improved.

### Demo

You can see screenrecord via this link: 
https://youtu.be/iQNpFI7LMsk

